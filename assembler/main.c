#include <stdlib.h>
#include "token.h"

int main(int argc, char *argv[])
{
    FILE *f = fopen(argv[1], "r");

    if(!f)
    	return 1;

    token_t *tokStack = (token_t*)malloc(sizeof(token_t) * 64);
    size_t numToks = 0;

    if(!tokenize_file(f, tokStack, &numToks))
        return -1;

    size_t i;

    for(i = 0; i < numToks; ++i) {
        switch(tokStack[i].type) {
            case TK_ID:
                switch(tokStack[i].u.id.type) {
                    case ID_DEF:
                        printf("Token %d: (Identifier) %s\n", i, tokStack[i].u.id.name);

                        break;

                    case ID_LBL:
                        printf("Token %d: (Label) %s\n", i, tokStack[i].u.id.name);

                        break;
                }

                break;

            case TK_NUM:
                printf("Token %d: (Number) %d\n", i, tokStack[i].u.num);

                break;
        }
    }

    free(tokStack);

    return 0;
}