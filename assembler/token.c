#include "token.h"

void parse_sentence(const char *s)
{
    char buffer[SENTENCE_MAX];
    strncpy(buffer, s, SENTENCE_MAX - 1);
    buffer[SENTENCE_MAX - 1] = 0;

    char *tok;

    printf("Tokenizing sentence \"%s\"\n", s);

    tok = strtok(buffer, " ,.-!?\n");
    while(tok) {
        printf("\t%s\n", tok);
        tok = strtok(NULL, " ,.-!?\n");
    }
}

inline int isendline(char c)
{
    if(c == '\n' || c == EOF)
        return 1;

    return 0;
}

inline int isendtok(char c)
{
    if(isspace(c) || isendline(c) || c == ',' || c == ';')
        return 1;

    return 0;
}

int tokenize_file(FILE *f, token_t *stack, size_t *n)
{
    if(!f || !stack) {
        fprintf(stderr, "You're joking, the file you passed or the token stack is NULL!\n");
        return;
    }

    enum STATE state = ST_WS;
    char cur = fgetc(f);
    int eof = 0;
    int line = 1;
    int col = 1;
    int expecting = 0;
    size_t stackIndex = -1;
    size_t idCharIndex = 0;

    while(!eof) {  
        if(cur == EOF)
            eof = 1; /* Allow one more loop after EOF so things can be processed */

        switch(state) {
            case ST_WS:
                if(cur == ';') {
                    state = ST_CM;
                    break;
                }

                if(isendline(cur) && expecting) {
                    fprintf(stderr, "[Line %d: %d] Error: expected identifier after ','\n", line, col);
                    return 0;
                }

                if(cur == ',' && expecting) {
                    fprintf(stderr, "[Line %d: %d] Error: unexpected separater ','\n", line, col);
                    return 0;
                }
                else if(cur == ',' && !expecting) {
                    expecting = 1;
                }

                if(cur == '\n') {
                    col = 0;
                    line++;
                }

                if(isalnum(cur)) {
                    state = ST_TK;
                    ungetc(cur, f); /* Unget the char because it is part of the identifier */
                    --col;
                    expecting = 0;
                    ++stackIndex;

                    break;
                }

                break;

            case ST_CM:
                if(isendline(cur)) {
                    state = ST_WS;
                    ungetc(cur, f); /* Possible newline, means we need to ++line */
                    --col;
                }

                break;

            case ST_TK:
                if(isalpha(cur)) {
                    state = ST_TK_ID;
                    stack[stackIndex].type = TK_ID;
                    stack[stackIndex].u.id.type = ID_DEF;
                    ungetc(cur, f);
                    --col;
                }
                else if(isdigit(cur)) {
                    state = ST_TK_NUM;
                    stack[stackIndex].type = TK_NUM;
                    stack[stackIndex].u.num = 0;
                    ungetc(cur, f);
                    --col;
                }

                break;

            case ST_TK_ID:
                if(idCharIndex > ID_NAME_MAX - 1) {
                    fprintf(stderr, "[Line %d: %d] Error: identifier exceeds max length (%d)\n", line, col, ID_NAME_MAX);
                    return 0;
                }

                if(isendtok(cur)) {
                    state = ST_WS;
                    stack[stackIndex].u.id.name[idCharIndex] = 0;
                    idCharIndex = 0;
                    ungetc(cur, f);
                    --col;

                    break;
                }
                else if(cur == ':') {
                    state = ST_WS;
                    stack[stackIndex].u.id.type = ID_LBL;
                    stack[stackIndex].u.id.name[idCharIndex] = 0;
                    idCharIndex = 0;

                    break;
                }

                if(!isalnum(cur)) {
                    fprintf(stderr, "[Line %d: %d] Error: encountered unexpected character '%c' in identifier name\n", line, col, cur);
                    return 0;
                }

                stack[stackIndex].u.id.name[idCharIndex++] = cur;

                break;

            case ST_TK_NUM:
                if(isendtok(cur)) {
                    state = ST_WS;
                    ungetc(cur, f);
                    --col;

                    break;
                }

                if(!isdigit(cur)) {
                    fprintf(stderr, "[Line %d: %d] Error: encountered unexpected character '%c' in number\n", line, col, cur);
                    return 0;
                }

                stack[stackIndex].u.num *= 10;
                stack[stackIndex].u.num += cur - '0';

                break;
        }

        cur = fgetc(f);

        ++col;
    }

    *n = stackIndex + 1;

    return 1; 
}

/*#define TOKEN_ID_LABEL 0
#define TOKEN_ID_ID 1

#define TOKEN_ID 0
#define TOKEN_NUM 1

#define STATE_WHITE 0
#define STATE_TOKEN 1
#define STATE_COMMENT 2
#define STATE_TOKEN_IDENTIFIER 3
#define STATE_TOKEN_DIGIT 4

#define COMMENT_DELIM ';'
#define SEPARATER_DELIM ','
#define LABEL_DELIM ':'

void parse_asm(const char *s)
{
    char buffer[PARSE_MAX];
    token_t tok[128];
    int state = STATE_WHITE;
    int expecting = 0;
    int end = 0;
    int i = 0;
    int j = 0;
    int k = 0;

    strncpy(buffer, s, PARSE_MAX - 1);

    while(!end) {
        switch(state) {
            case STATE_WHITE:
                end = !buffer[i];

                if((end || buffer[i] == '\n') && expecting) {
                    printf("Error: expected identifier after '%c'\n", SEPARATER_DELIM);
                    return;
                }

                if(buffer[i] == COMMENT_DELIM) {
                    state = STATE_COMMENT;
                    break;
                }

                if(isalnum(buffer[i])) {
                    expecting = 0;
                    state = STATE_TOKEN;
                    --i; 

                    break;
                }

                break;

            case STATE_COMMENT:
                if(buffer[i] == '\n') {
                    state = STATE_WHITE;
                }

                break;

            case STATE_TOKEN: 
                if(isalpha(buffer[i])) {
                    tok[j].type = TOKEN_ID;
                    tok[j].u.id.type = TOKEN_ID_ID; 
                    state = STATE_TOKEN_IDENTIFIER;
                    --i; 
                }
                else if(isdigit(buffer[i])) {
                    tok[j].type = TOKEN_NUM;
                    tok[j].u.num = 0;
                    state = STATE_TOKEN_DIGIT;
                    --i;
                }

                break;

            case STATE_TOKEN_IDENTIFIER:
                if(buffer[i] == ':') {
                    tok[j].u.id.type = TOKEN_ID_LABEL;
                    tok[j].u.id.name[k] = 0;
                    state = STATE_WHITE;
                    ++j;
                    k = 0;

                    break;
                }
                else if(isspace(buffer[i]) || buffer[i] == SEPARATER_DELIM || !buffer[i]) {

                    tok[j].u.id.type = TOKEN_ID_ID;
                    tok[j].u.id.name[k] = 0;
                    state = STATE_WHITE;
                    ++j;
                    k = 0;
                    expecting = buffer[i] == SEPARATER_DELIM;
                    end = !buffer[i];

                    break;
                }

                if(!isalnum(buffer[i])) {
                    printf("Error: expected alpha-numerical character in indentifier declaration\n");
                    return;
                }

                if(k > 127) {
                    printf("Error: identifier exceeded max characters (128)\n");
                    return;
                }

                tok[j].u.id.name[k] = buffer[i];

                ++k;

                break;

            case STATE_TOKEN_DIGIT:
                if(isspace(buffer[i]) || buffer[i] == SEPARATER_DELIM || !buffer[i]) {
                    state = STATE_WHITE;
                    ++j;
                    end = !buffer[i];
                    expecting = buffer[i] == SEPARATER_DELIM;

                    break;
                }

                if(!isdigit(buffer[i])) {
                    printf("Error: expected digit in number declaration\n");
                    return;
                }

                tok[j].u.num *= 10;
                tok[j].u.num += buffer[i] - '0';

                break;
        }

        ++i;
    }

    for(i = 0; i < j; ++i) {
        if(tok[i].type == TOKEN_ID) {
            if(tok[i].u.id.type == TOKEN_ID_LABEL)
                printf("Label: %s\n", tok[i].u.id.name);
            else
                printf("Identifier: %s\n", tok[i].u.id.name);
        }
        else {
            printf("Number: %d\n", tok[i].u.num);
        }
    }
} */

/*void parse_asmold2(const char *s)
{
    char buffer[PARSE_MAX];
    char identifier[PARSE_MAX];
    int number;
    int idType;
    int state = STATE_WHITE;
    int end = 0;
    strncpy(buffer, s, PARSE_MAX - 1);
    buffer[PARSE_MAX - 1] = 0;

    int i, j;

    i = 0;
    j = 0;

    while(!end) {
        switch(state) {
            case STATE_WHITE:
                if(buffer[i] == ';')
                    state = STATE_COMMENT;

                else if(!isspace(buffer[i]) && buffer[i] != ',' && buffer[i] != ':') {
                    state = STATE_TOKEN;
                    break;
                }

                ++i;

                break;

            case STATE_TOKEN:
                if(isspace(buffer[i]) || buffer[i] == ',' || buffer[i]) {
                    state = STATE_WHITE;
                    break;
                }
                else if(buffer[i] == ';') {
                    state = STATE_COMMENT;
                    break;
                }

                if(isalpha(buffer[i])) {
                    state = STATE_TOKEN_IDENTIFIER;
                    idType = TOKEN_ID_ID;
                    identifier[j++] = buffer[i++];

                    break;
                }
                else if(isdigit(buffer[i])) {
                    state = STATE_TOKEN_DIGIT;
                    number = buffer[i++] - '0';

                    break;
                }
                else {
                    printf("What the hell is this? %c\n", buffer[i]);
                }

                ++i;

                break;

            case STATE_TOKEN_IDENTIFIER:
                if(isspace(buffer[i]) || buffer[i] == ',' || !buffer[i + 1]) {
                    printf("Identifier: %s\n", identifier);
                    memset(identifier, 0, PARSE_MAX);
                    j = 0;
                    state = STATE_WHITE;
                    break;
                }
                else if(buffer[i] == ';') {
                    printf("Identifier: %s\n", identifier);
                    memset(identifier, 0, PARSE_MAX);
                    j = 0;
                    state = STATE_COMMENT;
                    break;
                }

                if(buffer[i] == ':') {
                    printf("Label: %s\n", identifier);
                    memset(identifier, 0, PARSE_MAX);
                    j = 0;
                    idType = TOKEN_ID_LABEL;

                    ++i;

                    state = STATE_WHITE;
                    break;
                }

                if(!isalnum(buffer[i])) {
                    printf("What the hell was that?\n");
                    return;
                }

                identifier[j++] = buffer[i++];

                break;

            case STATE_TOKEN_DIGIT:
                if(isspace(buffer[i]) || buffer[i] == ',' || buffer[i + 1] == 0) {
                    printf("Number: %d\n", number);
                    number = 0;
                    state = STATE_WHITE;
                    break;
                }
                else if(buffer[i] == ';') {
                    printf("Number: %d\n", number);
                    number = 0;
                    state = STATE_COMMENT;
                    break;
                }

                if(!isdigit(buffer[i])) {
                    printf("What the hell was that?\n");
                    return;
                }

                number *= 10;
                number += buffer[i++] - '0';

                break;

            case STATE_COMMENT:
                if(buffer[i] == '\n') {
                    state = STATE_WHITE;
                    break;
                }

                ++i;

                break;
        }

        //printf("STATE = %d\n", state);
    }
}*/

void parse_asm_old(const char *s)
{
    char buffer[SENTENCE_MAX];
    strncpy(buffer, s, SENTENCE_MAX - 1);
    buffer[SENTENCE_MAX - 1] = 0;

    char *tok;

    printf("Tokenizing line of assembly \"%s\"\n", s);

    tok = strtok(buffer, " ;,:");
    while(tok) {
        printf("\t%s\n", tok);
        tok = strtok(NULL, " ;,:");
    }
}