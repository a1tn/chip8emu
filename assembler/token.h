#ifndef C8ASM_TOKEN_H
#define C8ASM_TOKEN_H

#include <string.h>
#include <stdio.h>
#include <ctype.h>

#define SENTENCE_MAX 1024
#define PARSE_MAX 4096

#define ID_NAME_MAX 128

enum STATE
{
    ST_WS = 0,
    ST_CM,
    ST_TK,
    ST_TK_ID,
    ST_TK_NUM
};

enum TOKEN_TYPE
{
    TK_ID = 0,
    TK_NUM
};

enum IDENTIFIER_TYPE
{
    ID_DEF = 0,
    ID_LBL
};

typedef struct identifier
{
    char name[ID_NAME_MAX];
    enum IDENTIFIER_TYPE type;
} identifier_t;

typedef struct token
{
    enum TOKEN_TYPE type;
    union {
        identifier_t id;
        unsigned int num;
    } u;
} token_t;

void parse_sentence(const char *s);
int tokenize_file(FILE *f, token_t *stack, size_t *n);
void parse_asm(const char *s);
void parse_asm_old(const char *s);

#endif