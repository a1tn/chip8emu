#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

FILE *out = NULL;

void putop(uint16_t op);

/* Jumps to instruction # and adds 0x200 to the offset */
void c8u_jmp(uint16_t);
/* Clears the screen */
void c8_cls(void);
/* Returns from a subroutine */
void c8_ret(void);
/* Jumps to address */
void c8_jmp(uint16_t);
/* Calls subroutine at address */
void c8_call(uint16_t);
/* Skips the next instruction if reg == val */
void c8_se(uint8_t, uint8_t);
/* Skips the next instruction if reg != val */
void c8_sne(uint8_t, uint8_t);
/* Skips the next instruction if reg1 == reg2 */
void c8_ser(uint8_t, uint8_t);
/* Loads reg with val */
void c8_mov(uint8_t, uint8_t);
/* Adds val to reg */
void c8_add(uint8_t, uint8_t);
/* Loads reg1 with reg2 */
void c8_movr(uint8_t, uint8_t);
/* Loads reg1 with reg1 | reg2 */
void c8_or(uint8_t, uint8_t);
/* Loads reg1 with reg1 & reg2 */
void c8_and(uint8_t, uint8_t);
/* Loads reg1 with reg1 ^ reg2 */
void c8_xor(uint8_t, uint8_t);
/* Adds reg2 to reg1 */
void c8_addr(uint8_t, uint8_t);
/* Subtracts reg2 from reg1 */
void c8_sub(uint8_t, uint8_t);
/* Shifts reg1 right by 1 bit, the MSB is set to the bit that was removed */
void c8_shr(uint8_t, uint8_t);
/* Sets reg1 to reg2 - reg1 */
void c8_subn(uint8_t, uint8_t);
/* Shitfs reg1 left by 1 bit, the LSB is set to the bit that was removed */
void c8_shl(uint8_t, uint8_t);
/* Skips next instruction if reg1 != reg2 */
void c8_sner(uint8_t, uint8_t);
/* Loads I with address */
void c8_movi(uint16_t);
/* Jumps to address + V0 */
void c8_jmpa(uint16_t);
/* Sets reg to random byte & val */
void c8_rnd(uint8_t, uint8_t);
/* Draws sprite at reg1, reg2 with dimensions 8 * N */
void c8_drw(uint8_t, uint8_t, uint8_t);
/* Skips the next instruction if the key in reg is pressed */
void c8_sp(uint8_t);
/* Skips the next instruction if the key in reg is not pressed */
void c8_snp(uint8_t);
/* Loads DT into reg */
void c8_savedt(uint8_t);
/* Waits for a key press and stores the pressed key in reg */
void c8_wait(uint8_t);
/* Loads reg into DT */
void c8_loaddt(uint8_t);
/* Loads reg into ST */
void c8_loadst(uint8_t);
/* Adds reg to I */
void c8_addi(uint8_t);
/* Loads I with the address for the char contained in reg */ 
void c8_char(uint8_t);
/* Stores the BCD of reg in I ~ I + 2 */
void c8_bcd(uint8_t);
/* Stores registers 0 to val in I ~ I + val */
void c8_store(uint8_t);
/* Loads registers 0 to val from I ~ I + val */
void c8_load(uint8_t);

int main(int argc, char *argv[])
{
	out = fopen(argv[1], "wb");

	c8_cls();
	c8_mov(0, 0);
	c8_mov(1, 0);
	c8_mov(2, 0xB);
	c8_char(2);
	c8_drw(0, 1, 5);
	c8_mov(0, 5);
	c8_mov(1, 0);
	c8_mov(2, 0xA);
	c8_char(2);
	c8_drw(0, 1, 5);
	c8_mov(0, 10);
	c8_mov(1, 0);
	c8_mov(2, 0xD);
	c8_char(2);
	c8_drw(0, 1, 5);
	c8u_jmp(16);
}

void c8u_jmp(uint16_t ins)
{
	uint16_t addr = ins * 2 + 0x200;

	c8_jmp(addr);
}

inline void putop(uint16_t op)
{
	fputc((op & 0xFF00) >> 8, out);
	fputc(op & 0x00FF, out);
}

void c8_cls(void)
{
	putop(0x00E0);
}

void c8_ret(void)
{
	putop(0x00EE);
}

void c8_jmp(uint16_t addr)
{
	putop(0x1000 | (addr & 0x0FFF));
}

void c8_call(uint16_t addr)
{
	putop(0x2000 | (addr & 0x0FFF));
}

void c8_se(uint8_t reg, uint8_t val)
{
	putop(0x3000 | ((reg & 0x0F) << 8) | val);
}

void c8_sne(uint8_t reg, uint8_t val)
{
	putop(0x4000 | ((reg & 0x0F) << 8) | val);
}

void c8_ser(uint8_t reg1, uint8_t reg2)
{
	putop(0x5000 | ((reg1 & 0x0F) << 8) | ((reg2 & 0x0F) << 4) | 0x0);
}

void c8_mov(uint8_t reg, uint8_t val)
{
	putop(0x6000 | ((reg & 0x0F) << 8) | val);
}

void c8_add(uint8_t reg, uint8_t val)
{
	putop(0x7000 | ((reg & 0x0F) << 8) | val);
}

void c8_movr(uint8_t reg1, uint8_t reg2)
{
	putop(0x8000 | ((reg1 & 0x0F) << 8) | ((reg2 & 0x0F) << 4) | 0x0);
}

void c8_or(uint8_t reg1, uint8_t reg2)
{
	putop(0x8000 | ((reg1 & 0x0F) << 8) | ((reg2 & 0x0F) << 4) | 0x1);
}

void c8_and(uint8_t reg1, uint8_t reg2)
{
	putop(0x8000 | ((reg1 & 0x0F) << 8) | ((reg2 & 0x0F) << 4) | 0x2);
}

void c8_xor(uint8_t reg1, uint8_t reg2)
{
	putop(0x8000 | ((reg1 & 0x0F) << 8) | ((reg2 & 0x0F) << 4) | 0x3);
}

void c8_addr(uint8_t reg1, uint8_t reg2)
{
	putop(0x8000 | ((reg1 & 0x0F) << 8) | ((reg2 & 0x0F) << 4) | 0x4);
}

void c8_sub(uint8_t reg1, uint8_t reg2)
{
	putop(0x8000 | ((reg1 & 0x0F) << 8) | ((reg2 & 0x0F) << 4) | 0x5);
}

void c8_shr(uint8_t reg1, uint8_t reg2)
{
	putop(0x8000 | ((reg1 & 0x0F) << 8) | ((reg2 & 0x0F) << 4) | 0x6);
}

void c8_subn(uint8_t reg1, uint8_t reg2)
{
	putop(0x8000 | ((reg1 & 0x0F) << 8) | ((reg2 & 0x0F) << 4) | 0x7);
}

void c8_shl(uint8_t reg1, uint8_t reg2)
{
	putop(0x8000 | ((reg1 & 0x0F) << 8) | ((reg2 & 0x0F) << 4) | 0xE);
}

void c8_sner(uint8_t reg1, uint8_t reg2)
{
	putop(0x9000 | ((reg1 & 0x0F) << 8) | ((reg2 & 0x0F) << 4) | 0x0);
}

void c8_movi(uint16_t addr)
{
	putop(0xA000 | (addr & 0x0FFF));
}

void c8_jmpa(uint16_t addr)
{
	putop(0xB000 | (addr & 0x0FFF));
}

void c8_rnd(uint8_t reg, uint8_t val)
{
	putop(0xC000 | ((reg & 0x0F) << 8) | val);
}

void c8_drw(uint8_t regx, uint8_t regy, uint8_t rows)
{
	putop(0xD000 | ((regx & 0x0F) << 8) | ((regy & 0x0F) << 4) | rows);
}

void c8_sp(uint8_t reg)
{
	putop(0xE000 | ((reg & 0x0F) << 8) | 0x9E);
}

void c8_snp(uint8_t reg)
{
	putop(0xE000 | ((reg & 0x0F) << 8) | 0xA1);
}

void c8_savedt(uint8_t reg)
{
	putop(0xF000 | ((reg & 0x0F) << 8) | 0x07);
}

void c8_wait(uint8_t reg)
{
	putop(0xF000 | ((reg & 0x0F) << 8) | 0x0A);
}

void c8_loaddt(uint8_t reg)
{
	putop(0xF000 | ((reg & 0x0F) << 8) | 0x15);
}

void c8_loadst(uint8_t reg)
{
	putop(0xF000 | ((reg & 0x0F) << 8) | 0x18);
}

void c8_addi(uint8_t reg)
{
	putop(0xF000 | ((reg & 0x0F) << 8) | 0x1E);
}

void c8_char(uint8_t reg)
{
	putop(0xF000 | ((reg & 0x0F) << 8) | 0x29);
}

void c8_bcd(uint8_t reg)
{
	putop(0xF000 | ((reg & 0x0F) << 8) | 0x33);
}

void c8_store(uint8_t reg)
{
	putop(0xF000 | ((reg & 0x0F) << 8) | 0x55);
}

void c8_load(uint8_t reg)
{
	putop(0xF000 | ((reg & 0x0F) << 8) | 0x65);
}
