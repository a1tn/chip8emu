#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <stdio.h>
#include <time.h>

#include <SDL2/SDL.h>

#define SCREENSCALE 10

SDL_Window *_window = NULL;
SDL_Renderer *_renderer;

typedef uint16_t opcode;

opcode _curOp;

uint8_t _mem[4096];
uint8_t _regs[16];
uint8_t _keys[16];
uint8_t _framebuffer[64 * 32];
uint8_t _delayTimer;
uint8_t _soundTimer;
uint8_t _skip;
uint8_t _draw;
uint8_t _waitKey;
uint8_t _waitKeyReg;

uint16_t _i;
uint16_t _pc;
uint16_t _stack[16];
uint16_t _sp;

uint8_t _fontset[] = {
	0xF0, 0x90, 0x90, 0x90, 0xF0, /* 0 */
	0x20, 0x60, 0x20, 0x20, 0x70, /* 1 */
	0xF0, 0x10, 0xF0, 0x80, 0xF0, /* 2 */
	0xF0, 0x10, 0xF0, 0x10, 0xF0, /* 3 */
	0x90, 0x90, 0xF0, 0x10, 0x10, /* 4 */
	0xF0, 0x80, 0xF0, 0x10, 0xF0, /* 5 */
	0xF0, 0x80, 0xF0, 0x90, 0xF0, /* 6 */
	0xF0, 0x10, 0x20, 0x40, 0x40, /* 7 */
	0xF0, 0x90, 0xF0, 0x90, 0xF0, /* 8 */
	0xF0, 0x90, 0xF0, 0x10, 0xF0, /* 9 */
	0x60, 0x90, 0xF0, 0x90, 0x90, /* A */
	0xE0, 0x90, 0xE0, 0x90, 0xE0, /* B */
	0x70, 0x80, 0x80, 0x80, 0x70, /* C */
	0xE0, 0x90, 0x90, 0x90, 0xE0, /* D */
	0xF0, 0x80, 0xF0, 0x80, 0xF0, /* E */
	0xF0, 0x80, 0xE0, 0x80, 0x80, /* F */
};

uint8_t _keymap[] = { /* SDL to chip-8 keypad mapping */
	SDL_SCANCODE_1, SDL_SCANCODE_2, SDL_SCANCODE_3, SDL_SCANCODE_4,
	SDL_SCANCODE_Q, SDL_SCANCODE_W, SDL_SCANCODE_E, SDL_SCANCODE_R,
	SDL_SCANCODE_A, SDL_SCANCODE_S, SDL_SCANCODE_D, SDL_SCANCODE_F,
	SDL_SCANCODE_Z, SDL_SCANCODE_X, SDL_SCANCODE_C, SDL_SCANCODE_V
};

void init_video()
{
	if(SDL_Init(SDL_INIT_EVERYTHING) < 0) {
		fprintf(stderr, "Error: could not initialize SDL: %s\n", SDL_GetError());

		exit(1);
	}

	_window = SDL_CreateWindow("chip8emu", 
							   SDL_WINDOWPOS_UNDEFINED, 
							   SDL_WINDOWPOS_UNDEFINED, 
							   64 * SCREENSCALE, 
							   32 * SCREENSCALE, 
							   SDL_WINDOW_SHOWN);

	if(_window == NULL) {
		fprintf(stderr, "Error: could not create window: %s\n", SDL_GetError());

		exit(1);
	}

	_renderer = SDL_CreateRenderer(_window, -1, SDL_RENDERER_ACCELERATED);

	if(_renderer == NULL) {
		fprintf(stderr, "Error: could not create renderer: %s\n", SDL_GetError());

		exit(1);
	}

	SDL_SetRenderDrawColor(_renderer, 0xFF, 0xFF, 0xFF, 0xFF);
}

void kill_video()
{
	SDL_DestroyWindow(_window);
	SDL_Quit();
}

inline void clear_framebuffer()
{
	uint16_t i;

	for(i = 0; i < 2048; ++i)
		_framebuffer[i] = 0;
}

void draw_screen()
{
	SDL_Rect r;
	r.w = SCREENSCALE;
	r.h = SCREENSCALE;
	r.x = 0;
	r.y = 0;

	int i, j;

	for(j = 0; j < 32; ++j) {
		for(i = 0; i < 64; ++i) {
			r.x = i * SCREENSCALE;
			r.y = j * SCREENSCALE;

			if(_framebuffer[j * 64 + i]) {
				SDL_RenderFillRect(_renderer, &r);
			}
		}
	}
}

inline void load_fontset()
{
	uint16_t i;

	for(i = 0; i < sizeof(_fontset); ++i)
		_mem[i] = _fontset[i];
}

void draw_sprite(uint8_t x, uint8_t y, uint8_t n)
{
	uint8_t f = 0;
	uint8_t i, j;

	for(j = 0; j < n; ++j) {
		uint8_t p = _mem[_i + j];

		for(i = 0; i < 8; ++i) {
			if((p & (0x80 >> i)) != 0) {
				if(_framebuffer[((y + j) * 64) + x + i])
					f = 1;

				_framebuffer[((y + j) * 64) + x + i] ^= 1;
			}
		}
	}

	_regs[15] = f;
}

void store_regs(uint8_t n)
{
	uint8_t i;

	for(i = 0; i < n; ++i)
		_mem[_i + i] = _regs[i];
}

void load_regs(uint8_t n)
{
	uint8_t i;

	for(i = 0; i < n; ++i)
		_regs[i] = _mem[_i + i];

}

void dump_regs()
{
	uint8_t i;

	printf("chip8emu register dump:\n");

	for(i = 0; i < 16; ++i) {
		printf("\tV[%d]: 0x%X\n", i, _regs[i]);
	}

	printf("\n\tI = 0x%X\n", _i);
	printf("\tPC = 0x%X\n", _pc);
	printf("\tSP = 0x%X\n", _sp);

}

void dump_mem()
{
	uint16_t i;

	printf("chip8emu memory dump:\n");

	for(i = 0; i < 4096; ++i) {
		printf("\t0x%X:\t0x%X\n", i, _mem[i]);
	}
}

void init()
{
	srand(time(NULL));

	_pc = 0x200;
	_curOp = 0;
	_sp = 0;
	_skip = 0;
	_draw = 0;
	_waitKey = 0;
	_waitKeyReg = 0;
	_soundTimer = 0;
	_delayTimer = 0;
	
	int i;
	for(i = 0; i < 16; ++i)
		_regs[i] = _keys[i] = _stack[i] = 0;

	load_fontset();

	clear_framebuffer();

	printf("chip8emu initialized.\n");
}

void load_rom(const char *file)
{
	FILE *romFile = fopen(file, "rb");
	long romSize;
	uint8_t *buffer;
	int i;
	if(romFile == NULL) {
		fprintf(stderr, "Error: could not open ROM \"%s\"\n", file);
		exit(1);
	}

	fseek(romFile, 0, SEEK_END);
	romSize = ftell(romFile);
	rewind(romFile);

	buffer = (uint8_t*)malloc(sizeof(uint8_t)*romSize);

	if(buffer == NULL) {
		fprintf(stderr, "Error: Could not allocate memory for ROM\n");
		exit(1);
	}

	fread(buffer, 1, romSize, romFile);

	printf("chip8emu: Loading ROM into memory:\n");

	for(i = 0; i < romSize; ++i) {
		_mem[i + 0x200] = buffer[i];
	}

	free(buffer);
	fclose(romFile);
}

void decode_op()
{
	switch(_curOp & 0xF000) {
		case 0x0000:
			if(_curOp == 0x00E0) {
				clear_framebuffer();
				_pc += 2;
			}
			else if(_curOp == 0x00EE) {
				_pc = _stack[--_sp];
			}
			else {
				fprintf(stderr, "Error: chip8emu does not suport calling RCA 1802 programs\n");
				_pc += 2;
				//exit(1);
			}
			break;

		case 0x1000:
			_pc = _curOp & 0x0FFF;
			break;

		case 0x2000:
			_stack[_sp++] = _pc;
			_pc = _curOp & 0x0FFF;
			break;

		case 0x3000:
			if(_regs[(_curOp & 0x0F00) >> 8] == (_curOp & 0x00FF))
				_skip = 1;
			else
				_skip = 0;

			_pc += 2;

			break;

		case 0x4000:
			if(_regs[(_curOp & 0x0F00) >> 8] != (_curOp & 0x00FF))
				_skip = 1;
			else
				_skip = 0;

			_pc += 2;

			break;

		case 0x5000:
			if(_regs[(_curOp & 0x0F00) >> 8] == _regs[(_curOp & 0x00F0) >> 4])
				_skip = 1;
			else
				_skip = 0;

			_pc += 2;

			break;

		case 0x6000:
			_regs[(_curOp & 0x0F00) >> 8] = _curOp & 0x00FF;

			_pc += 2;

			break;

		case 0x7000:
			_regs[(_curOp & 0x0F00) >> 8] += _curOp & 0x00FF;

			_pc += 2;

			break;

		case 0x8000:
			switch(_curOp & 0x000F) {
				case 0x0000:
					_regs[(_curOp & 0x0F00) >> 8] = _regs[(_curOp & 0x00F0) >> 4];

					break;

				case 0x0001:
					_regs[(_curOp & 0x0F00) >> 8] = _regs[(_curOp & 0x0F00) >> 8] | _regs[(_curOp & 0x00F0) >> 4];

					break;

				case 0x0002:
					_regs[(_curOp & 0x0F00) >> 8] = _regs[(_curOp & 0x0F00) >> 8] & _regs[(_curOp & 0x00F0) >> 4];

					break;

				case 0x0003:
					_regs[(_curOp & 0x0F00) >> 8] = _regs[(_curOp & 0x0F00) >> 8] ^ _regs[(_curOp & 0x00F0) >> 4];

					break;

				case 0x0004:
					if(_regs[(_curOp & 0x00F0) >> 4] > 0xFF - _regs[(_curOp & 0x0F00) >> 8])
						_regs[15] = 1;
					else
						_regs[15] = 0;

					_regs[(_curOp & 0x0F00) >> 8] += _regs[(_curOp & 0x00F0) >> 4];

					break;

				case 0x0005:
					if(_regs[(_curOp & 0x00F0) >> 4] > _regs[(_curOp & 0x0F00) >> 8])
						_regs[15] = 0;
					else
						_regs[15] = 1;

					_regs[(_curOp & 0x0F00) >> 8] -= _regs[(_curOp & 0x00F0) >> 4];

					break;

				case 0x0006:
					_regs[15] = _regs[(_curOp & 0x0F00) >> 8] & 0x0001;
					_regs[(_curOp & 0x0F00) >> 8] >>= 1;

					break;

				case 0x0007:
					if(_regs[(_curOp & 0x0F00) >> 8] > _regs[(_curOp & 0x00F0)] >> 4)
						_regs[15] = 0;
					else
						_regs[15] = 1;

					_regs[(_curOp & 0x0F00) >> 8] = _regs[(_curOp & 0x00F0) >> 4] - _regs[(_curOp & 0x0F00) >> 8];

					break;

				case 0x000E:
					_regs[15] = _regs[(_curOp & 0x0F00) >> 8] & (1 << 7);
					_regs[(_curOp & 0x0F00) >> 8] <<= 1;

					break;

				default:
					fprintf(stderr, "Error: encountered unknown opcode: 0x%X\n", _curOp);
					//exit(1);

					break;
			}

			_pc += 2;

			break;

		case 0x9000:
			if(_regs[(_curOp & 0x0F00) >> 8] != _regs[(_curOp & 0x00F0) >> 4])
				_skip = 1;
			else
				_skip = 0;

			_pc += 2;

			break;

		case 0xA000:
			_i = _curOp & 0x0FFF;

			_pc += 2;

			break;

		case 0xB000:
			_pc = (_curOp & 0x0FFF) + _regs[0];

			break;

		case 0xC000:
			_regs[(_curOp & 0x0F00) >> 8] = (rand() % 256) & (_curOp & 0x00FF);

			_pc += 2;

			break;

		case 0xD000:
			draw_sprite(_regs[(_curOp & 0x0F00) >> 8], _regs[(_curOp &0x00F0) >> 4], (_curOp & 0x000F));

			_draw = 1;
			_pc += 2;

			break;

		case 0xE000:
			switch(_curOp & 0x00FF) {
				case 0x009E:
					if(_keys[_regs[(_curOp & 0x0F00) >> 8]])
						_skip = 1;
					else
						_skip = 0;

					break;

				case 0x00A1:
					if(!_keys[_regs[(_curOp & 0x0F00) >> 8]])
						_skip = 1;
					else
						_skip = 0;

					break;

				default:
					fprintf(stderr, "Error: encountered unknown opcode: 0x%X\n", _curOp);
					//exit(1);
					break;
			}

			_pc += 2;

			break;

		case 0xF000:
			switch(_curOp & 0x00FF) {
				case 0x0007:
					_regs[(_curOp & 0x0F00) >> 8] = _delayTimer;

					break;

				case 0x000A:
					_waitKey = 1;
					_waitKeyReg = (_curOp & 0x0F00) >> 8; 
					//fprintf(stderr, "Sorry, wait for keypress not yet implemented.\n");
					//exit(1);

					break;

				case 0x0015:
					_delayTimer = _regs[(_curOp & 0x0F00) >> 8];

					break;

				case 0x0018:
					_soundTimer = _regs[(_curOp & 0x0F00) >> 8];

					break;

				case 0x001E:
					_i += _regs[(_curOp & 0x0F00) >> 8];

					break;

				case 0x0029:
					_i = _regs[(_curOp & 0x0F00) >> 8] * 5;

					break;

				case 0x0033:
					_mem[_i] 	 = _regs[(_curOp & 0x0F00) >> 8] / 100;
					_mem[_i + 1] = (_regs[(_curOp & 0x0F00) >> 8] / 10) % 10;
					_mem[_i + 2] = (_regs[(_curOp & 0x0F00) >> 8] % 100) % 10;

					break; 

				case 0x0055:
					store_regs((_curOp & 0x0F00) >> 8);

					break;

				case 0x0065:
					load_regs((_curOp & 0x0F00) >> 8);

					break;
			}

			_pc += 2;

			break;

		default:
			fprintf(stderr, "Error: encountered unkown opcode: 0x%X\n", _curOp);
			//exit(1);
			_pc += 2;
			break;
	}
}

void disassemble_op()
{
	switch(_curOp & 0xF000) {
		case 0x0000:
			if(_curOp == 0x00E0) {
				printf("%.4X: cls\n", _pc);
			}
			else if(_curOp == 0x00EE) {
				printf("%.4X: ret\n", _pc);
			}
			else {
				/* printf("%.4X: nop\n", _pc); */
			}
			break;

		case 0x1000:
			printf("%.4X: jmp 0x%.4X\n", _pc, _curOp & 0x0FFF);
			break;

		case 0x2000:
			printf("%.4X: call 0x%.4X\n", _pc, _curOp & 0x0FFF);
			break;

		case 0x3000:
			printf("%.4X: se V%.1X, 0x%.2X\n", _pc, (_curOp & 0x0F00) >> 8, _curOp & 0x00FF);
			break;

		case 0x4000:
			printf("%.4X: sne V%.1X, 0x%.2X\n", _pc, (_curOp & 0x0F00) >> 8, _curOp & 0x00FF);
			break;

		case 0x5000:
			printf("%.4X: se V%.1X, V%.1X\n", _pc, (_curOp & 0x0F00) >> 8, (_curOp & 0x00F0) >> 4);
			break;

		case 0x6000:
			printf("%.4X: mov V%.1X, 0x%.2X\n", _pc, (_curOp & 0x0F00) >> 8, _curOp & 0x00FF);
			break;

		case 0x7000:
			printf("%.4X: add V%.1X, 0x%.2X\n", _pc, (_curOp & 0x0F00) >> 8, _curOp & 0x00FF);
			break;

		case 0x8000:
			switch(_curOp & 0x000F) {
				case 0x0000:
					printf("%.4X: mov V%.1X, V%.1X\n", _pc, (_curOp & 0x0F00) >> 8, (_curOp & 0x00F0) >> 4);
					break;

				case 0x0001:
					printf("%.4X: or V%.1X, V%.1X\n", _pc, (_curOp & 0x0F00) >> 8, (_curOp & 0x00F0) >> 4);
					break;

				case 0x0002:
					printf("%.4X: and V%.1X, V%.1X\n", _pc, (_curOp & 0x0F00) >> 8, (_curOp & 0x00F0) >> 4);
					break;

				case 0x0003:
					printf("%.4X: xor V%.1X, V%.1X\n", _pc, (_curOp & 0x0F00) >> 8, (_curOp & 0x00F0) >> 4);
					break;

				case 0x0004:
					printf("%.4X: addc V%.1X, V%.1X\n", _pc, (_curOp & 0x0F00) >> 8, (_curOp & 0x00F0) >> 4);
					break;

				case 0x0005:
					printf("%.4X: subc V%.1X, V%.1X\n", _pc, (_curOp & 0x0F00) >> 8, (_curOp & 0x00F0) >> 4);
					break;

				case 0x0006:
					printf("%.4X: shr V%.1X\n", _pc, (_curOp & 0x0F00) >> 8);
					break;

				case 0x0007:
					printf("%.4X: movsubc V%.1X, V%.1X\n", _pc, (_curOp & 0x0F00) >> 8, (_curOp & 0x00F0) >> 4);
					break;

				case 0x000E:
					printf("%.4X: shl V%.1X\n", _pc, (_curOp & 0x0F00) >> 8);

					break;

				default:
					fprintf(stderr, "Error: encountered unknown opcode: 0x%X\n", _curOp);
					//exit(1);

					break;
			}
			break;

		case 0x9000:
			printf("%.4X: sne V%.1X, V%.1X\n", _pc, (_curOp & 0x0F00) >> 8, (_curOp & 0x00F0) >> 4);
			break;

		case 0xA000:
			printf("%.4X: mov I, 0x%.4X\n", _pc, _curOp & 0x0FFF);
			break;

		case 0xB000:
			printf("%.4X: jmpa 0x%.4X\n", _pc, _curOp & 0x0FFF);
			break;

		case 0xC000:
			printf("%.4X: rnd V%.1X, 0x%.2X\n", _pc, (_curOp & 0x0F00) >> 8, _curOp & 0x00FF);
			break;

		case 0xD000:
			printf("%.4X: drw V%.1X, V%.1X, 0x%.1X\n", _pc, (_curOp & 0x0F00) >> 8, (_curOp & 0x00F0) >> 4, _curOp & 0x000F);
			break;

		case 0xE000:
			switch(_curOp & 0x00FF) {
				case 0x009E:
					printf("%.4X: sp V%.1X\n", _pc, (_curOp & 0x0F00) >> 8);
					break;

				case 0x00A1:
					printf("%.4X: snp V%.1X\n", _pc, (_curOp & 0x0F00) >> 8);
					break;

				default:
					fprintf(stderr, "Error: encountered unknown opcode: 0x%X\n", _curOp);
					//exit(1);
					break;
			}
			break;

		case 0xF000:
			switch(_curOp & 0x00FF) {
				case 0x0007:
					printf("%.4X: mov V%.1X, DT\n", _pc, (_curOp & 0x0F00) >> 8);
					break;

				case 0x000A:
					printf("%.4X: wait V%.1X\n", _pc, (_curOp & 0x0F00) >> 8);
					break;

				case 0x0015:
					printf("%.4X: mov DT, V%.1X\n", _pc, (_curOp & 0x0F00) >> 8);
					break;

				case 0x0018:
					printf("%.4X: mov ST, V%.1X\n", _pc, (_curOp & 0x0F00) >> 8);
					break;

				case 0x001E:
					printf("%.4X: add I, V%.1X\n", _pc, (_curOp & 0x0F00) >> 8);
					break;

				case 0x0029:
					printf("%.4X: char V%.1X\n", _pc, (_curOp & 0x0F00) >> 8);
					break;

				case 0x0033:
					printf("%.4X: bcd V%.1X\n", _pc, (_curOp & 0x0F00) >> 8);
					break; 

				case 0x0055:
					printf("%.4X: store V%.1X\n", _pc, (_curOp & 0x0F00) >> 8);
					break;

				case 0x0065:
					printf("%.4X: load V%.1X\n", _pc, (_curOp & 0x0F00) >> 8);
					break;
			}
			break;

		default:
			fprintf(stderr, "Error: encountered unkown opcode: 0x%X\n", _curOp);
			//exit(1);
			break;
	}
}


void next_op()
{
	_curOp = _mem[_pc] << 8 | _mem[_pc + 1];

	//printf("Read opcode 0x%X from _mem[0x%X]\n", _curOp, _pc);

	if(_skip) {
		_pc += 2;
		_skip = 0;
	}
	else
		decode_op();
}

void update_timers()
{
	if(_delayTimer > 0)
		--_delayTimer;

	if(_soundTimer > 0) {
		if(--_soundTimer == 0)
			printf("boop cat on nose to establish dominance\n");
	}
}

void cycle()
{
	if(_draw) {
		_draw = 0;

		SDL_SetRenderDrawColor(_renderer, 0x00, 0x00, 0x00, 0xFF);
		SDL_RenderClear(_renderer);

		SDL_SetRenderDrawColor(_renderer, 0xFF, 0xFF, 0xFF, 0xFF);

		draw_screen();

		SDL_RenderPresent(_renderer);
	}

	next_op();

	update_timers();
}

int main(int argc, char *argv[])
{
	int open = 1;

	init();
	init_video();

	load_rom(argv[1]);

	while(_pc < 0x1000) {
		_curOp = _mem[_pc] << 8 | _mem[_pc + 1];

		disassemble_op();
		_pc += 2;
	}

	_pc = 0x200;

	_curOp = 0;

	while(open) {
		SDL_Event e;
		while(SDL_PollEvent(&e) != 0) {
			if(e.type == SDL_QUIT)
				open = 0;

			else if(e.type == SDL_KEYDOWN) {
				int i;
				for(i = 0; i < 16; ++i) {
					if(e.key.keysym.scancode == _keymap[i]) {
						if(_waitKey) {
							_waitKey = 0;
							_regs[_waitKeyReg] = i;
							_waitKeyReg = 0;
						}
						printf("%s pressed\n", SDL_GetScancodeName(e.key.keysym.scancode));
						_keys[i] = 1;
					}
				}
			}
			else if(e.type == SDL_KEYUP) {
				int i;
				for(i = 0; i < 16; ++i) {
					if(e.key.keysym.scancode == _keymap[i]) {
						printf("%s released\n", SDL_GetScancodeName(e.key.keysym.scancode));
						_keys[i] = 0;
					}
				}
			}
		}

		SDL_Delay(3);

		if(!_waitKey)
			cycle();
	}

	return 0;
}