#include <stdio.h>

int main(int argc, char *argv[]) {
	FILE *o = fopen(argv[1], "wb");

	if(!o) {
		printf("Failed to open \"%s\" for writing.\n", argv[1]);

		return 1;
	}

	int i;
	for(i = 0x200; i < 0x1000; ++i)
		fputc(0, o);

	printf("ROM file \"%s\" with default size created.\n", argv[1]);

	return 0;
}